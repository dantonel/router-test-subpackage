import React from 'react';

import { Switch, Route, BrowserRouter } from 'react-router-dom';

class Test extends React.PureComponent
{
  render()
  {
    return (
      <BrowserRouter>
        <Switch>
          { this.props.custom_routes }
          <Route exact path="/main" component={ () => <div>Test</div> }/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Test;
