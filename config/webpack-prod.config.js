'use strict';

const merge = require('webpack-merge');
const webpackCommon = require('./webpack-common.config');
const path = require('path');

module.exports = merge(webpackCommon, {
  mode: 'production',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '../dist'),
    libraryTarget: 'commonjs2',
  },
  externals: {
    'react': 'commonjs react',
    'react-dom': 'commonjs react-dom',
  },

  devtool: 'none',
});
