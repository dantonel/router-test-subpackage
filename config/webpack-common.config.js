'use strict';

module.exports =
{
  stats: {
    entrypoints: false,
    children: false,
    modules: false,
  },
  entry: {
    index: './src/test.jsx'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
};
