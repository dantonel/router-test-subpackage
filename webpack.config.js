'use strict';

module.exports = env =>
{
    const config = {
        dev: [
            require('./config/webpack-dev.config'), // dev files
        ],
        prod: [
            require('./config/webpack-prod.config'), // lib files
        ],
    };

    return config[env];
};
